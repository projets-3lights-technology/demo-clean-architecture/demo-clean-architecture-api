# Demo Clean Architecture Backend

[![pipeline status](https://gitlab.com/mickaelw/demo-clean-architecture-backend/badges/master/pipeline.svg)](https://gitlab.com/mickaelw/demo-clean-architecture-backend/commits/master)

### Installation

```
yarn or npm i
```

### Launch test

```
yarn test or npm test
```

### Launch server
```
- yarn start:inmemory or npm start:inmemory
- yarn start or npm start
```