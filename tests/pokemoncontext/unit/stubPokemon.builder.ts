import uuidv4 from 'uuid/v4'
import {PokemonBuilder} from '../../../src/pokemoncontext/usecases/Pokemon.builder'

export class StubPokemonBuilder extends PokemonBuilder {

    protected _id: String = uuidv4()
    protected _name: String = "pikachu"
    protected _picture: String = "picture"
    protected _type: String = "souris"
    protected _height: number = 0.4
    protected _weight: number = 6.0
    protected _description: String = "Chaque fois que Pikachu découvre quelque chose de nouveau, il envoie un arc électrique. Lorsqu'on tombe sur une Baie carbonisée, ça signifie sans doute qu'un de ces Pokémon a envoyé une charge trop forte."

}