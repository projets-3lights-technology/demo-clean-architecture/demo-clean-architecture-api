import {expect} from 'chai'
import {PokemonsHandler} from '../../../src/pokemoncontext/usecases/pokemons.handler'
import {FetchAllPokemons} from '../../../src/pokemoncontext/usecases/queries/fetchAllPokemon.query'
import {Pokemon} from '../../../src/pokemoncontext/domain/entities/pokemon'
import {InMemoryPokemonRepository} from '../../../src/pokemoncontext/adapters/secondaries/inmemory/inMemoryPokemon.repository'
import {GetPokemonById} from '../../../src/pokemoncontext/usecases/queries/getPokemonById.query'
import {NotFoundPokemonError} from '../../../src/pokemoncontext/domain/errors/notFoundPokemon.error'
import {StubPokemonBuilder} from './stubPokemon.builder'
import {PokemonRepository} from '../../../src/pokemoncontext/domain/repositories/pokemon.repository'

const chai = require('chai')
const sinonChai = require('sinon-chai')
chai.use(sinonChai)

describe('Pokemons handler', () => {

    context('Can fetch a list of pokemons', () => {

        it('With zero pokemon if there is no pokemon in the resource', () => {
            const pokemonHandler: PokemonsHandler = createPokemonHandler()

            const pokemonList = pokemonHandler.query(new FetchAllPokemons())

            expect(pokemonList).deep.equal([])
        })

        it('With some pokemons if there are pokemons in the resource', () => {
            const expectedPokemonList: Pokemon[] = [
                new StubPokemonBuilder().withName("pickachu").build(),
                new StubPokemonBuilder().withName("salameche").build()
            ]
            const pokemonHandler: PokemonsHandler = createPokemonHandler(expectedPokemonList)

            const pokemonList = pokemonHandler.query(new FetchAllPokemons())

            verifyPokemonsList(pokemonList, expectedPokemonList)
        })

        function verifyPokemonsList(pokemonList: Pokemon[], expectedPokemonList: Pokemon[]) {
            expect(pokemonList.length).equal(expectedPokemonList.length)
            expectedPokemonList.forEach((pokemon, index) => verifyOnePokemon(pokemon, pokemonList[index]))
        }

    })

    context('Can get all information about one pokemon', () => {

        it('If the pokemon exists', () => {
            const pikachu = new StubPokemonBuilder().withName("pickachu").build()
            const salameche = new StubPokemonBuilder().withId("2").withName("salameche").build()
            const expectedPokemonList: Pokemon[] = [pikachu, salameche]
            const pokemonHandler: PokemonsHandler = createPokemonHandler(expectedPokemonList)

            const pokemon = pokemonHandler.query(new GetPokemonById("2"))[0]

            verifyOnePokemon(pokemon, salameche)
        })

        it('Inform if no pokemon founded', () => {
            const pokemonHandler: PokemonsHandler = createPokemonHandler()

            const pokemon = () => pokemonHandler.query(new GetPokemonById("2"))

            expect(pokemon).to.throw("Pokemon 2 not found").instanceOf(NotFoundPokemonError)
        })

    })

    function createPokemonHandler(pokemonPopulation: Pokemon[] = []): PokemonsHandler {
        const pokemonRepository: PokemonRepository = new InMemoryPokemonRepository(pokemonPopulation)
        return new PokemonsHandler(pokemonRepository)
    }

    function verifyOnePokemon(pokemon: Pokemon, expectedPokemon: Pokemon) {
        expect(pokemon.id).equal(expectedPokemon.id)
        expect(pokemon.name).equal(expectedPokemon.name)
        expect(pokemon.picture).equal(expectedPokemon.picture)
        expect(pokemon.type).equal(expectedPokemon.type)
        expect(pokemon.height).equal(expectedPokemon.height)
        expect(pokemon.weight).equal(expectedPokemon.weight)
        expect(pokemon.description).equal(expectedPokemon.description)
    }

})
