import {expect} from 'chai'
import {StubPokemonBuilder} from './stubPokemon.builder'
import {PokemonPresenter} from '../../../src/pokemoncontext/adapters/primaries/presenters/pokemon.presenter'
import {PokemonPresented} from '../../../src/pokemoncontext/adapters/primaries/presenters/pokemonPresented'

const chai = require('chai')
const sinonChai = require('sinon-chai')
chai.use(sinonChai)

describe('Pokemons presenter', () => {

    it('Present pokemon resource', () => {
        const pokemon = new StubPokemonBuilder()
            .withId("1")
            .withName("pikachu")
            .withPicture("picture")
            .withHeight(2)
            .withWeight(3)
            .withDescription("Lorem ipsum")
            .build()

        const pokemonPresented = PokemonPresenter.present(pokemon)
        const expectedPokemonPresented: PokemonPresented = {
            id: "1",
            name: "pikachu",
            picture_url: "picture",
            height: 2,
            weight: 3,
            description: "Lorem ipsum"
        }

        expect(pokemonPresented).deep.equal(expectedPokemonPresented)
    })

})
