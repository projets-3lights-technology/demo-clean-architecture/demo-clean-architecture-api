import {fetchAllPokemon} from '../../adapters/primaries/REST/fetchAllPokemon'
import {pokemonContextDI} from '../pokemonContext.DI'
import {getPokemonById} from '../../adapters/primaries/REST/getPokemonById'

const cors = require('cors')

export const pokemonContextRoutes = app => {
    app.options('*', cors())
    app.get("/api/v1/pokemons", fetchAllPokemon(pokemonContextDI.pokemonHandler))
    app.get("/api/v1/pokemons/:id", getPokemonById(pokemonContextDI.pokemonHandler))
}