import {PokemonSchema} from '../../adapters/primaries/graphQL/pokemonQuery.schema'

const rootPokemonContextSchema = `
    extend type Query {
        pokemons: [Pokemon]
        pokemon(id: String!): Pokemon
    }
`

export const PokemonContextSchema = () => [
    rootPokemonContextSchema,
    PokemonSchema
]