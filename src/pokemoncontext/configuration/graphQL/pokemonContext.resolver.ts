import merge from 'lodash.merge'
import {pokemonContextDI} from '../pokemonContext.DI'
import {fetchAllPokemons} from '../../adapters/primaries/graphQL/fetchAllPokemonsQuery.resolver'
import {getPokemonById} from '../../adapters/primaries/graphQL/getPokemonByIdQuery.resolver'

export const PokemonContextResolver = merge({},
    fetchAllPokemons(pokemonContextDI.pokemonHandler),
    getPokemonById(pokemonContextDI.pokemonHandler)
)
