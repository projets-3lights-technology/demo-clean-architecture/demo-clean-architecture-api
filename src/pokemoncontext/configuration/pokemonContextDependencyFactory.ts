import {InMemoryPokemonRepository} from '../adapters/secondaries/inmemory/inMemoryPokemon.repository'
import {PokemonRepository} from '../domain/repositories/pokemon.repository'
import {StubPokemonBuilder} from '../../../tests/pokemoncontext/unit/stubPokemon.builder'

export class PokemonContextDependencyFactory {

    static pokemonRepository(): PokemonRepository {
        switch (process.env.ENV) {
            default:
                const bulbizarre = new StubPokemonBuilder()
                    .withId("1")
                    .withName("fdsqgfdh")
                    .withType("Plante")
                    .withPicture("https://assets.pokemon.com/assets/cms2/img/pokedex/full/001.png")
                    .withHeight(0.7)
                    .withWeight(6.9)
                    .withDescription("Bulbizarre passe son temps à faire la sieste sous le soleil. Il y a une graine sur son dos. Il absorbe les rayons du soleil pour faire doucement pousser la graine.")
                    .build()
                const salameche = new StubPokemonBuilder()
                    .withId("4")
                    .withName("salameche")
                    .withType("Feu")
                    .withPicture("https://assets.pokemon.com/assets/cms2/img/pokedex/full/004.png")
                    .withHeight(0.4)
                    .withWeight(8.5)
                    .withDescription("La flamme qui brûle au bout de sa queue indique l'humeur de ce Pokémon. Elle vacille lorsque Salamèche est content. En revanche, lorsqu'il s'énerve, la flamme prend de l'importance et brûle plus ardemment.")
                    .build()
                const pikachu = new StubPokemonBuilder()
                    .withId("25")
                    .withName("pikachu")
                    .withType("Electrique")
                    .withPicture("https://assets.pokemon.com/assets/cms2/img/pokedex/full/025.png")
                    .withHeight(0.4)
                    .withWeight(6.0)
                    .withDescription("Chaque fois que Pikachu découvre quelque chose de nouveau, il envoie un arc électrique. Lorsqu'on tombe sur une Baie carbonisée, ça signifie sans doute qu'un de ces Pokémon a envoyé une charge trop forte.")
                    .build()
                const mewtwo = new StubPokemonBuilder()
                    .withId("150")
                    .withName("mewtwo")
                    .withType("Psy")
                    .withPicture("https://assets.pokemon.com/assets/cms2/img/pokedex/full/150.png")
                    .withHeight(2.0)
                    .withWeight(122.0)
                    .withDescription("Mewtwo est un Pokémon créé par manipulation génétique. Cependant, bien que les connaissances scientifiques des humains aient réussi à créer son corps, elles n'ont pas pu doter Mewtwo d'un cœur sensible.")
                    .build()

                return new InMemoryPokemonRepository([
                    bulbizarre,
                    salameche,
                    pikachu,
                    mewtwo
                ])
        }
    }

}