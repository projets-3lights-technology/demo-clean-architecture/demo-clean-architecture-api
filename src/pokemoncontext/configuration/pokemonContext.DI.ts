import {PokemonRepository} from '../domain/repositories/pokemon.repository'
import {PokemonsHandler} from '../usecases/pokemons.handler'
import {PokemonContextDependencyFactory} from './pokemonContextDependencyFactory'

const pokemonRepository: PokemonRepository = PokemonContextDependencyFactory.pokemonRepository()
const pokemonHandler: PokemonsHandler = new PokemonsHandler(pokemonRepository)

export const pokemonContextDI = {
    pokemonHandler
}