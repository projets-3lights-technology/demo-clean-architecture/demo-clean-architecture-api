import {FetchAllPokemons} from '../../../usecases/queries/fetchAllPokemon.query'
import {PokemonPresenter} from '../presenters/pokemon.presenter'
import {PokemonsHandler} from '../../../usecases/pokemons.handler'
import {map} from 'ramda'

const Query = (pokemonHandler: PokemonsHandler) => ({
    pokemons: () => {
        return Promise.resolve(
            map(
                PokemonPresenter.present,
                pokemonHandler.query(new FetchAllPokemons())
            )
        )
    }
})

export const fetchAllPokemons = (pokemonHandler: PokemonsHandler) => ({
    Query: Query(pokemonHandler)
})