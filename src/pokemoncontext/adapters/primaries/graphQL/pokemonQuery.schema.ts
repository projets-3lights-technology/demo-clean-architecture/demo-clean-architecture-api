const Pokemon = `
    type Pokemon {
        id: String
        name: String
        description: String
        height: Float
        weight: Float
        picture_url: String
    }
`

export const PokemonSchema = () => [
    Pokemon
]