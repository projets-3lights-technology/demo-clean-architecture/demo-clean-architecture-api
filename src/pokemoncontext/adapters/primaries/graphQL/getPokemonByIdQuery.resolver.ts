import {PokemonPresenter} from '../presenters/pokemon.presenter'
import {PokemonsHandler} from '../../../usecases/pokemons.handler'
import {head, map} from 'ramda'
import {GetPokemonById} from '../../../usecases/queries/getPokemonById.query'

const Query = (pokemonHandler: PokemonsHandler) => ({
    pokemon: (data, {id}) => {
        return Promise.resolve(
            head(map(PokemonPresenter.present, pokemonHandler.query(new GetPokemonById(id))))
        )
    },
})

export const getPokemonById = (pokemonHandler: PokemonsHandler) => ({
    Query: Query(pokemonHandler)
})