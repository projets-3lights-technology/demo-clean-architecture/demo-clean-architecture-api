import {Pokemon} from '../../../domain/entities/pokemon'
import {PokemonPresented} from './pokemonPresented'

export class PokemonPresenter {

    static present(pokemon: Pokemon): PokemonPresented {
        return {
            id: pokemon.id,
            name: pokemon.name,
            picture_url: pokemon.picture,
            height: pokemon.height,
            weight: pokemon.weight,
            description: pokemon.description
        }
    }
}