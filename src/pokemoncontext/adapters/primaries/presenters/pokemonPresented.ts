export interface PokemonPresented {
    id: String
    name: String
    picture_url: String
    height: number
    weight: number
    description: String
}