import {Request, Response} from 'express'
import {PokemonsHandler} from '../../../usecases/pokemons.handler'
import {PokemonPresenter} from '../presenters/pokemon.presenter'
import {GetPokemonById} from '../../../usecases/queries/getPokemonById.query'
import {map, head} from 'ramda'

export const getPokemonById = (pokemonHandler: PokemonsHandler) =>
    (req: Request, res: Response) => {
        try {
            const pokemonPresented = head(map(PokemonPresenter.present, pokemonHandler.query(new GetPokemonById(req.params.id))))

            res.send(pokemonPresented)
        } catch (e) {
            res.status(404).send(e.message)
        }
    }