import {Request, Response} from "express";
import {PokemonsHandler} from '../../../usecases/pokemons.handler'
import {FetchAllPokemons} from '../../../usecases/queries/fetchAllPokemon.query'
import {PokemonPresenter} from '../presenters/pokemon.presenter'
import {map} from 'ramda'

export const fetchAllPokemon = (pokemonHandler: PokemonsHandler) =>
    (req: Request, res: Response) => {
        const pokemonsPresented = map(
            PokemonPresenter.present,
            pokemonHandler.query(new FetchAllPokemons())
        )

        res.send(pokemonsPresented)
    }