import { PokemonRepository } from '../../../domain/repositories/pokemon.repository'
import { Pokemon } from '../../../domain/entities/pokemon'
import { filter, propEq } from 'ramda'

export class InMemoryPokemonRepository implements PokemonRepository {

    constructor(private pokemons: Pokemon[] = []) {
    }

    all(): Pokemon[] {
        return this.pokemons
    }

    get(id: String): Pokemon[] {
        return filter(propEq('id', id), this.pokemons)
    }

}