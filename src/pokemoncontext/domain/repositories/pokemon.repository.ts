import {Pokemon} from '../entities/pokemon'

export interface PokemonRepository {

    all(): Pokemon[]

    get(id: String): Pokemon[]

}