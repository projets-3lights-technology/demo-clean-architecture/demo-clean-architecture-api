export class Pokemon {

    constructor(private _id: String,
                private _name: String,
                private _picture: String,
                private _type: String,
                private _height: number,
                private _weight: number,
                private _description: String) {
    }

    get id(): String {
        return this._id
    }

    get name(): String {
        return this._name
    }

    get picture(): String {
        return this._picture
    }

    get type(): String {
        return this._type
    }

    get height(): number {
        return this._height
    }

    get weight(): number {
        return this._weight
    }

    get description(): String {
        return this._description
    }

}