export class NotFoundPokemonError extends Error {

    constructor(message: string) {
        super(message)
    }

}