import {Pokemon} from '../entities/pokemon'
import {PokemonRepository} from '../repositories/pokemon.repository'

export interface QueryCommand {

    query(pokemonRepository: PokemonRepository): Pokemon[]

}