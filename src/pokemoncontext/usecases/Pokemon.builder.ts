import {Pokemon} from '../domain/entities/pokemon'

export class PokemonBuilder {

    protected _id: String
    protected _name: String
    protected _picture: String
    protected _type: String
    protected _height: number
    protected _weight: number
    protected _description: String

    withId(value: String): PokemonBuilder {
        this._id = value
        return this
    }

    withName(value: String): PokemonBuilder {
        this._name = value
        return this
    }

    withPicture(value: String): PokemonBuilder {
        this._picture = value
        return this
    }

    withType(value: String): PokemonBuilder {
        this._type = value
        return this
    }

    withHeight(value: number): PokemonBuilder {
        this._height = value
        return this
    }

    withWeight(value: number): PokemonBuilder {
        this._weight = value
        return this
    }

    withDescription(value: String): PokemonBuilder {
        this._description = value
        return this
    }

    build(): Pokemon {
        return new Pokemon(
            this._id,
            this._name,
            this._picture,
            this._type,
            this._height,
            this._weight,
            this._description
        )
    }

}