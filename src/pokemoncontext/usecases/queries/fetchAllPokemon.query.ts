import {QueryCommand} from '../../domain/commands/query.command'
import {Pokemon} from '../../domain/entities/pokemon'
import {PokemonRepository} from '../../domain/repositories/pokemon.repository'

export class FetchAllPokemons implements QueryCommand{

    query(pokemonRepository: PokemonRepository): Pokemon[] {
        return pokemonRepository.all()
    }

}