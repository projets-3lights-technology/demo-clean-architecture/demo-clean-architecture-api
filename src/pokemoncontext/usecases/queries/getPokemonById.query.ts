import {QueryCommand} from '../../domain/commands/query.command'
import {PokemonRepository} from '../../domain/repositories/pokemon.repository'
import {Pokemon} from '../../domain/entities/pokemon'
import {NotFoundPokemonError} from '../../domain/errors/notFoundPokemon.error'

export class GetPokemonById implements QueryCommand {

    constructor(private id: String) {
    }

    query(pokemonRepository: PokemonRepository): Pokemon[] {
        const pokemon = pokemonRepository.get(this.id)
        if (pokemon.length <= 0)
            throw new NotFoundPokemonError("Pokemon " + this.id + " not found")

        return pokemon
    }

}