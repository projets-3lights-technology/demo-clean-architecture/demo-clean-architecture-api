import {PokemonRepository} from '../domain/repositories/pokemon.repository'
import {QueryCommand} from '../domain/commands/query.command'

export class PokemonsHandler {

    constructor(private pokemonRepository: PokemonRepository) {
    }

    query(command: QueryCommand) {
        return command.query(this.pokemonRepository)
    }

}