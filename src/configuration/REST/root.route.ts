import {pokemonContextRoutes} from '../../pokemoncontext/configuration/REST/pokemonContext.routes'

export const rootRoute = app => pokemonContextRoutes(app)
