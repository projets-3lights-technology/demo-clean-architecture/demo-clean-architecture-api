import {PokemonContextResolver} from '../../pokemoncontext/configuration/graphQL/pokemonContext.resolver'
import merge from 'lodash.merge'

export const rootGraphQLResolver = merge({},
    PokemonContextResolver
)