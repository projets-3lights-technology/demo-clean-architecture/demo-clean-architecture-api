import {PokemonContextSchema} from '../../pokemoncontext/configuration/graphQL/pokemonContext.schema'

const rootQuerySchema = `
    type Query {
        _empty: String
    }
`

const rootMutationSchema = `
    type Mutation {
        _empty: String
    }
`

export const rootGraphQLSchema = [
    rootQuerySchema,
    rootMutationSchema,
    PokemonContextSchema
]