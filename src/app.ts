import express from 'express'
import bodyParser from 'body-parser'
import dotenv from 'dotenv'
import {graphqlExpress, graphiqlExpress} from 'apollo-server-express'
import {makeExecutableSchema} from 'graphql-tools'

import {rootRoute} from './configuration/REST/root.route'
import {rootGraphQLSchema} from './configuration/graphQL/root.schema'
import {rootGraphQLResolver} from './configuration/graphQL/root.resolver'

const cors = require('cors')

dotenv.config({path: process.env.ENVFILE || '.env.inmemory'})

const app = express()

app.set('port', process.env.PORT || 8000)
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(cors())


app.use(
    '/graphql',
    bodyParser.json(),
    graphqlExpress({
        schema: makeExecutableSchema({
            typeDefs: rootGraphQLSchema,
            resolvers: rootGraphQLResolver
        }),
    })
)
app.get('/graphiql', graphiqlExpress({endpointURL: '/graphql'}))

rootRoute(app)

export default app